FROM python:3.9-buster

ARG PYTHON_ENV=development

ENV POETRY_VERSION=1.1.12 \
  POETRY_VIRTUALENVS_CREATE=false

RUN pip install "poetry==${POETRY_VERSION}"

WORKDIR /code

COPY ./poetry.lock ./pyproject.toml /code/

RUN poetry install $(test ${PYTHON_ENV} == production && echo "--no-dev")

COPY ./book_telegram /code/book_telegram

CMD ["poetry", "run", "start"]
