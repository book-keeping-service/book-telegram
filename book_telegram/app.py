import logging

from aiogram import Dispatcher
from aiogram.utils import executor

import book_telegram.handlers  # noqa
from book_telegram.loader import dp


logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
)

logger.info("Starting bot")


async def on_shutdown(dispatcher: Dispatcher):
    await dispatcher.storage.close()
    await dispatcher.storage.wait_closed()


def start():
    executor.start_polling(dp, on_shutdown=on_shutdown)


if __name__ == "__main__":
    start()
