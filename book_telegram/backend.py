import aiohttp
from yarl import URL

from book_telegram.settings import settings


class Backend:
    base_url: URL

    @classmethod
    def get_new_session(cls) -> aiohttp.ClientSession:
        return aiohttp.ClientSession(base_url=cls.base_url)

    def __init__(self):
        self.session = self.get_new_session()

    def get_session(self) -> aiohttp.ClientSession:
        if self.session and not self.session.closed:
            return self.session
        self.session = self.get_new_session()

        return self.session


class BookBackend(Backend):
    base_url = URL(settings.backend_base_url)


class GoogleBooksBackend(Backend):
    base_url = URL("https://www.googleapis.com/")


book_backend = BookBackend()
google_api_backend = GoogleBooksBackend()
