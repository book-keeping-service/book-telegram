class EditingOtherPersonBook(Exception):
    """Raises when someone trying to change data of other person's book"""


class AlreadyRegisteredOnBook(Exception):
    """Raises when user trying to register on book that he already registered"""


class UnexpectedBackendError(Exception):
    """Raises when backend responses with unexpected status code"""
