import re
from typing import List, Optional

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import StatesGroup, State

from book_telegram.exceptions import EditingOtherPersonBook, AlreadyRegisteredOnBook
from book_telegram.loader import dp, bot
from book_telegram.models.book import (
    BookInstance,
    Book,
    StatusEnum,
    BookInstanceCreate,
    BookCreate,
)
from book_telegram.models.user import User
from book_telegram.services.book import (
    get_available_book_instances_from_backend,
    create_book_type_on_backend,
    get_book_data_by_isbn_from_backend,
    create_book_instance_on_backend,
    get_book_info_by_isbn_from_google_api,
    get_book_info_by_id,
    get_books_by_user,
    borrow_book,
    pass_book_on,
    get_book_instance_by_id,
    remove_book_instance,
)
from book_telegram.services.user import get_user_by_telegram_uid
from book_telegram.utils import (
    mention_telegram_user_in_markdown,
    convert_isbn_to_numbers,
)


def create_inline_message_keyboard(callback_value: int) -> types.InlineKeyboardMarkup:
    keyboard = types.InlineKeyboardMarkup()

    keyboard.add(
        types.InlineKeyboardButton(
            text="Забронировать", callback_data=f"book_{str(callback_value)}"
        )
    )

    return keyboard


@dp.message_handler(Text(equals="📚 Список книг"))
async def get_list_of_available_books(msg: types.Message):
    books: List[Book] = await get_available_book_instances_from_backend()

    if len(books) == 0:
        await msg.reply(
            "Пока что нет доступных книг. Но вы всегда можете её добавить", reply=False
        )

    for book in books:
        await msg.reply(
            f"📕 {book.author} «{book.name}»\n" f"📚 Количество экземпляров: {book.qty}",
            reply=False,
            reply_markup=create_inline_message_keyboard(book.id),
        )


class AddBook(StatesGroup):
    waiting_for_isbn = State()
    waiting_for_author = State()
    waiting_for_title = State()


@dp.message_handler(commands=["cancel"], state=AddBook.all_states)
async def cancel_add_book(msg: types.Message, state: FSMContext):
    await state.finish()


@dp.message_handler(commands=["back"], state=AddBook.all_states)
async def previous_step_add_book(msg: types.Message, state: FSMContext):
    phrase = ""
    current_state = await state.get_state()
    if current_state == AddBook.waiting_for_isbn.state:
        return
    if current_state == AddBook.waiting_for_author.state:
        phrase = "💬 Введите ISBN в 13-значном формате.\n"
    if current_state == AddBook.waiting_for_title.state:
        phrase = "✍ Укажите автора книги\n"

    phrase += (
        "/back - для возврата на предыдущий шаг\n" "/cancel - для отмены добавления"
    )
    await msg.answer(phrase)
    await AddBook.previous()


@dp.message_handler(Text(equals="➕ Добавить книгу"))
async def add_new_book(msg: types.Message):
    await AddBook.waiting_for_isbn.set()
    await msg.answer(
        "💬 Введите ISBN в 13-значном формате.\n"
        "/back - для возврата на предыдущий шаг\n"
        "/cancel - для отмены добавления",
        reply=False,
    )


@dp.message_handler(state=AddBook.waiting_for_isbn)
async def check_isbn(msg: types.Message, state: FSMContext):
    if not re.fullmatch(
        pattern=r"^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$", string=msg.text
    ):
        await msg.answer("🛑 Введите корректный ISBN", reply=False)
        return

    formatted_isbn = convert_isbn_to_numbers(msg.text)

    await state.update_data(isbn=formatted_isbn)

    user: User = await get_user_by_telegram_uid(str(msg.from_user.id))
    await msg.reply("⚙ Смотрю, не добавляли ли эту книгу ранее...", reply=False)
    book_data: Optional[Book] = await get_book_data_by_isbn_from_backend(formatted_isbn)

    if book_data:
        book_instance_to_create = BookInstanceCreate(
            status=StatusEnum.available, book_id=book_data.id, owner_id=user.id
        )
        book = await create_book_instance_on_backend(book_instance_to_create)
        await state.finish()
        if book:
            await msg.reply("✅ Книга успешно добавлена", reply=False)
            return
        await msg.reply(
            "❌ Возникла проблема при добавлении книги в коллекцию", reply=False
        )
        return

    await msg.reply("⚙ Ищу данные на Google API...", reply=False)
    book_data: Optional[Book] = await get_book_info_by_isbn_from_google_api(
        formatted_isbn
    )
    if book_data:
        book_type: Book = await create_book_type_on_backend(book_data)
        book_instance_to_create = BookInstanceCreate(
            status=StatusEnum.available, book_id=book_type.id, owner_id=user.id
        )
        book = await create_book_instance_on_backend(book_instance_to_create)
        await state.finish()
        if book:
            await msg.reply("✅ Книга успешно добавлена", reply=False)
            return
        await msg.reply(
            "❌ Возникла проблема при добавлении книги в коллекцию", reply=False
        )
        return

    await msg.reply(
        "😥 Не удалось найти информацию о книге. Придется заполнить информацию вручную.",
        reply=False,
    )
    await AddBook.next()
    await msg.reply(
        "✍ Укажите автора книги\n"
        "/back - для возврата на предыдущий шаг\n"
        "/cancel - для отмены добавления",
        reply=False,
    )


@dp.message_handler(state=AddBook.waiting_for_author)
async def author_filled(msg: types.Message, state: FSMContext):
    await state.update_data(author=msg.text)
    await AddBook.next()

    await msg.reply(
        "🧾 Введите название книги\n"
        "/back - для возврата на предыдущий шаг\n"
        "/cancel - для отмены добавления",
        reply=False,
    )


@dp.message_handler(state=AddBook.waiting_for_title)
async def title_filled(msg: types.Message, state: FSMContext):
    await state.update_data(name=msg.text)

    user: User = await get_user_by_telegram_uid(str(msg.from_user.id))

    book_type_to_create = BookCreate(**(await state.get_data()))
    book_type_on_backend = await create_book_type_on_backend(book_type_to_create)
    book_instance_to_create = BookInstanceCreate(
        status=StatusEnum.available, owner_id=user.id, book_id=book_type_on_backend.id
    )
    book = await create_book_instance_on_backend(book_instance_to_create)
    await state.finish()
    if book:
        await msg.reply("✅ Книга успешно добавлена", reply=False)
        return
    await msg.reply("❌ Возникла проблема при добавлении книги в коллекцию", reply=False)


async def send_notification_message_to_book_owner(book: BookInstance, message: str):
    """
    Отправляет уведомление владельцу книги
    :param book: экземпляр книги
    :param message: сообщение
    :return:
    """
    await bot.send_message(
        chat_id=int(book.owner.telegram_uid),
        text=message,
    )


@dp.callback_query_handler(Text(startswith="book_"))
async def handle_book_borrowing(callback_query: types.CallbackQuery):
    book_id: int = int(callback_query.data.split("_")[-1])

    book_from_backend = await get_book_info_by_id(book_id)

    try:
        is_borrowed, book_after_update = await borrow_book(
            book_from_backend.id, str(callback_query.from_user.id)
        )
    except AlreadyRegisteredOnBook:
        return await callback_query.answer(
            "❌ Вы уже оформили подписку на эту книгу", show_alert=True
        )

    if is_borrowed:
        await bot.send_message(
            callback_query.from_user.id,
            "✨ Отлично! Нашли для вас свободную книгу. Теперь осталось связаться с {0} для получения книги".format(
                mention_telegram_user_in_markdown(
                    full_name=book_after_update.owner.name,
                    telegram_id=book_after_update.owner.telegram_uid,
                )
            ),
        )

        # Отправляем владельцу книги уведомление о необходимости связаться с человеком
        await send_notification_message_to_book_owner(
            book_after_update,
            "{0} хочет почитать книгу {1}.".format(
                mention_telegram_user_in_markdown(
                    full_name=callback_query.from_user.full_name,
                    telegram_id=str(callback_query.from_user.id),
                ),
                book_after_update.book.name,
            ),
        )
    else:
        await bot.send_message(
            callback_query.from_user.id,
            "❌ К сожалению, все экземпляры этой книги сейчас заняты.\n"
            "Я оповещу тебя, как только появится свободный экземпляр.",
        )

    await callback_query.answer()


@dp.message_handler(Text(equals="📖 Мои книги"))
async def reply_all_borrowed_books(msg: types.Message):
    book_instances: List[BookInstance] = await get_books_by_user(str(msg.from_user.id))
    if len(book_instances) == 0:
        await msg.reply("🤷‍♂️ У вас нет ни 1 книги.", reply=False)

    for book_instance in book_instances:
        is_owner = book_instance.owner.telegram_uid == str(msg.from_user.id)
        await msg.reply(
            f"📕 {book_instance.book.author} «{book_instance.book.name}»\n"
            f"⚙ Статус: {book_instance.status}\n",
            f"",
            reply=False,
            reply_markup=create_book_management_inline_keyboard(
                book_instance.id,
                is_available=book_instance.status == StatusEnum.available,
                is_owner=is_owner,
                self_borrow=book_instance.borrower
                and book_instance.borrower.telegram_uid == str(msg.from_user.id),
            ),
        )


def create_book_management_inline_keyboard(
    book_id: int,
    is_available: bool,
    is_owner: bool,
    self_borrow: bool,
) -> types.InlineKeyboardMarkup:
    keyboard = types.InlineKeyboardMarkup()
    buttons = []

    if is_owner:
        buttons.append(
            types.InlineKeyboardButton(
                text="Изъять", callback_data=f"withdraw_book_{str(book_id)}"
            )
        )

        if not is_available:
            buttons.append(
                types.InlineKeyboardButton(
                    text="Где книга?", callback_data=f"search_book_{str(book_id)}"
                )
            )
        if self_borrow:
            buttons.append(
                types.InlineKeyboardButton(
                    text="Передать", callback_data=f"return_book_{str(book_id)}"
                )
            )
    else:
        buttons.append(
            types.InlineKeyboardButton(
                text="Передать", callback_data=f"return_book_{str(book_id)}"
            )
        )

    keyboard.add(*buttons)

    return keyboard


@dp.callback_query_handler(Text(startswith="return_book_"))
async def handle_book_returning_to_owner(callback_query: types.CallbackQuery):
    book_id: int = int(callback_query.data.split("_")[-1])
    user: User = await get_user_by_telegram_uid(str(callback_query.from_user.id))

    try:
        is_owner, passed_on_user, book_info = await pass_book_on(
            book_id, user.telegram_uid
        )
    except EditingOtherPersonBook:
        await callback_query.answer(
            "❌ Не удалось вернуть книгу владельцу. Возможно, у неё сменился владелец",
            show_alert=True,
        )
        return

    if passed_on_user:
        await bot.send_message(
            callback_query.from_user.id,
            "✅ Теперь свяжитесь с {0} и передайте ему книгу".format(
                mention_telegram_user_in_markdown(
                    full_name=passed_on_user.name,
                    telegram_id=passed_on_user.telegram_uid,
                )
            ),
        )

        # Отправляем сообщение получателю книги
        if is_owner:
            await bot.send_message(
                int(passed_on_user.telegram_uid),
                '🏁 Привет! Книгу "{0}" прочитали все желающие, и теперь она готова вернуться к вам.\n'
                "Свяжитесь с {1} для уточнения деталей передачи книги".format(
                    book_info.name,
                    mention_telegram_user_in_markdown(
                        full_name=callback_query.from_user.full_name,
                        telegram_id=str(callback_query.from_user.id),
                    ),
                ),
            )
        else:
            await bot.send_message(
                passed_on_user.telegram_uid,
                "👋 Привет! Пришла твоя очередь прочитать книгу {0}.\n"
                "В ближайшее время {1} свяжется с тобой для уточнения деталей передачи книги.".format(
                    book_info.name,
                    mention_telegram_user_in_markdown(
                        full_name=callback_query.from_user.full_name,
                        telegram_id=str(callback_query.from_user.id),
                    ),
                ),
            )

    await callback_query.answer()


@dp.callback_query_handler(Text(startswith="withdraw_book_"))
async def handle_book_withdraw(callback_query: types.CallbackQuery):
    book_id: int = int(callback_query.data.split("_")[-1])

    borrowed_user = await remove_book_instance(book_id)

    if borrowed_user:
        await bot.send_message(
            int(borrowed_user.telegram_uid),
            "😥 К сожалению, автор книги ({0}) решил изъять книгу из библиотеки.\n"
            "Свяжитесь с автором для передачи книги.".format(
                mention_telegram_user_in_markdown(
                    full_name=callback_query.from_user.full_name,
                    telegram_id=str(callback_query.from_user.id),
                )
            ),
        )
        await bot.send_message(
            callback_query.from_user.id,
            "✅ Мы отправили запрос пользователю {0} на возврат книги.\n"
            "Скоро он свяжется с вами для уточнения деталей передачи книги".format(
                mention_telegram_user_in_markdown(
                    full_name=borrowed_user.name, telegram_id=borrowed_user.telegram_uid
                )
            ),
        )
    else:
        await bot.send_message(
            callback_query.from_user.id, "✅ Книга успешно изъята из библиотеки."
        )

    await callback_query.answer()


@dp.callback_query_handler(Text(startswith="search_book_"))
async def handle_search_book(callback_query: types.CallbackQuery):
    book_id: int = int(callback_query.data.split("_")[-1])
    book_instance = await get_book_instance_by_id(book_id)

    if book_instance.borrower:
        await bot.send_message(
            callback_query.from_user.id,
            "📖 Сейчас книга у {0}".format(
                mention_telegram_user_in_markdown(
                    full_name=book_instance.borrower.name,
                    telegram_id=book_instance.borrower.telegram_uid,
                )
            ),
        )

    await callback_query.answer()
