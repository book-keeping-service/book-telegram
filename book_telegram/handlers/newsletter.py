import warnings

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.utils.exceptions import BotBlocked

from book_telegram.loader import dp, bot
from book_telegram.services.newsletter import NewsletterService
from book_telegram.services.user import create_initial_keyboard


class SendNotification(StatesGroup):
    waiting_for_content = State()


@dp.message_handler(commands=["notify"])
async def notify_users(msg: types.Message):
    await SendNotification.waiting_for_content.set()
    await msg.answer("Введите сообщение.\n/cancel для отмены действия.")


@dp.message_handler(state=SendNotification.waiting_for_content)
async def check_message(msg: types.Message, state: FSMContext):
    if msg.text == "/cancel":
        return await state.finish()

    service = NewsletterService()
    mailing_list = await service.get_mailing_list()

    for user in mailing_list:
        try:
            await bot.send_message(
                int(user.telegram_uid), msg.text, reply_markup=create_initial_keyboard()
            )
        except BotBlocked:
            warnings.warn("Пользователь {0} заблокировал бота".format(user.name))

    await state.finish()
