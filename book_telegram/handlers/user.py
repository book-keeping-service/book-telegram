from aiogram.types import Message

from book_telegram.loader import dp
from book_telegram.services.user import register_new_user, create_initial_keyboard


@dp.message_handler(commands=["start"])
async def user_start(msg: Message):
    await register_new_user(
        {"name": msg.from_user.full_name, "telegram_uid": msg.from_user.id}
    )

    await msg.reply(
        "👋 Привет, используй команды у клавиатуры для взаимодействия",
        reply_markup=create_initial_keyboard(),
        reply=False,
    )
