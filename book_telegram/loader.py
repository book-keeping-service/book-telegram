from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from book_telegram.settings import bot_settings


bot = Bot(**bot_settings.dict())

dp = Dispatcher(bot, storage=MemoryStorage())
