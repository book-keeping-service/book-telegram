import enum
from typing import Optional

from pydantic import BaseModel

from book_telegram.models.user import User


class BookCreate(BaseModel):
    name: str
    author: str
    isbn: str
    description: Optional[str] = None


class Book(BookCreate):
    id: Optional[int] = None
    qty: Optional[int] = None


class StatusEnum(str, enum.Enum):
    available = "Доступна"
    loaned = "Взята в аренду"


class BookInstanceCreate(BaseModel):
    status: StatusEnum
    owner_id: int
    book_id: int
    loaned_by: Optional[int] = None


class BookInstance(BookInstanceCreate):
    id: int
    book: Book
    owner: User
    borrower: Optional[User] = None
