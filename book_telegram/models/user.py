from pydantic import BaseModel


class UserCreate(BaseModel):
    name: str
    telegram_uid: str


class User(UserCreate):
    id: int
