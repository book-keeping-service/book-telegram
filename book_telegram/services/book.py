import logging
import warnings
from typing import List, Optional, Tuple, Union

from aiohttp import ContentTypeError
from pydantic import ValidationError
from yarl import URL

from book_telegram.backend import book_backend, google_api_backend
from book_telegram.exceptions import (
    EditingOtherPersonBook,
    AlreadyRegisteredOnBook,
    UnexpectedBackendError,
)
from book_telegram.models.book import Book, BookInstance, BookCreate, BookInstanceCreate
from book_telegram.models.user import User
from book_telegram.settings import bot_settings


async def get_book_data_by_isbn_from_backend(isbn: str) -> Book:
    session = book_backend.get_session()

    async with session.get(f"/references/book?isbn={isbn}") as response:
        if response.status == 200:
            return Book.parse_obj(await response.json())


async def get_available_book_instances_from_backend() -> List[Book]:
    session = book_backend.get_session()

    async with session.get("/references/book/?available=true") as response:
        if response.status != 200:
            print(await response.json())
            logging.error(
                "Произошла неизвестная ошибка на бэкенде при попытке получения списка доступных книг"
            )
            return []

        return [Book.parse_obj(book) for book in await response.json()]


async def create_book_type_on_backend(book_type_data: BookCreate) -> Optional[Book]:
    session = book_backend.get_session()

    async with session.post(
        "/references/book/", json=book_type_data.dict()
    ) as response:
        if not response.status == 201:
            logging.error(f"Ошибка при создании типа книги")
            return

        return Book.parse_obj(await response.json())


async def create_book_instance_on_backend(
    book_data: BookInstanceCreate,
) -> BookInstance:
    session = book_backend.get_session()

    async with session.post("/books", json=book_data.dict()) as response:
        if not response.status == 201:
            logging.error("Ошибка при добавлении экземпляра книги")

        return BookInstance.parse_obj(await response.json())


async def get_book_info_by_isbn_from_google_api(isbn: str) -> Optional[Book]:
    session = google_api_backend.get_session()

    async with session.get(
        URL("/books/v1/volumes").with_query({"q": f"isbn:{isbn}"}),
        proxy=bot_settings.proxy,
        proxy_auth=bot_settings.proxy_auth,
    ) as response:
        if response.status != 200:
            logging.error("Произошла ошибка при получении данных с Google API")
            return

        payload: dict = await response.json()

        if payload.get("totalItems") == 0:
            return

        book_dict: dict = payload.get("items")[0].get("volumeInfo")
        return Book(
            name=book_dict.get("title"),
            author=book_dict.get("authors")[0],
            isbn=isbn,
        )


async def get_books_by_user(telegram_uid: str) -> List[BookInstance]:
    session = book_backend.get_session()

    async with session.get(f"/books?telegram_uid={telegram_uid}") as response:
        if response.status != 200:
            warnings.warn("Не удалось получить данные с бэкенда")
            return []

        return [BookInstance.parse_obj(book) for book in await response.json()]


async def get_book_info_by_id(book_id: int) -> Optional[Book]:
    session = book_backend.get_session()

    async with session.get(f"/references/book?book_id={book_id}") as response:
        if response.status != 200:
            warnings.warn("Не удалось найти экземпляр книги с таким id")
            return
        return Book.parse_obj(await response.json())


async def get_book_instance_by_id(book_instance_id: int) -> BookInstance:
    session = book_backend.get_session()

    async with session.get(
        f"/books/{book_instance_id}", raise_for_status=True
    ) as response:
        return BookInstance.parse_obj(await response.json())


async def remove_book_instance(book_instance_id: int) -> Optional[User]:
    session = book_backend.get_session()

    async with session.delete(
        f"/books/{book_instance_id}", raise_for_status=True
    ) as response:
        try:
            payload = await response.json()
        except ContentTypeError:
            return

        return User.parse_obj(payload)


async def update_book_instance_data(book_data: BookInstance) -> BookInstance:
    session = book_backend.get_session()

    async with session.patch(
        f"/books/{book_data.id}",
        json=book_data.dict(exclude_unset=True, exclude={"id"}),
    ) as response:
        if response.status != 200:
            raise UnexpectedBackendError(
                f"Не удалось обновить информацию о книге\n{await response.json()}"
            )

        return BookInstance.parse_obj(await response.json())


async def borrow_book_instance(
    book: BookInstance, user_id: int
) -> Optional[BookInstance]:
    book.status = "Взята в аренду"

    if book.loaned_by:
        return

    book.loaned_by = user_id
    return await update_book_instance_data(book)


async def borrow_book(
    book_id: int, telegram_id: str
) -> Tuple[bool, Union[BookInstance, List[User]]]:
    session = book_backend.get_session()

    async with session.post(
        f"/references/book/{book_id}/borrow",
        headers={"x-telegram-id": telegram_id},
    ) as response:
        if response.status == 400:
            raise AlreadyRegisteredOnBook()
        payload = await response.json()

        if payload.get("book_instance"):
            return True, BookInstance.parse_obj(payload.get("book_instance"))

        return False, [User.parse_obj(user) for user in payload.get("queue")]


async def return_book_instance_to_owner(
    book: BookInstance, user_id: int
) -> BookInstance:
    book.status = "Доступна"

    if book.loaned_by != user_id:
        raise EditingOtherPersonBook

    book.loaned_by = None

    return await update_book_instance_data(book)


async def pass_book_on(book_id: int, telegram_uid: str) -> Tuple[bool, User, Book]:
    session = book_backend.get_session()

    async with session.patch(
        f"/books/{book_id}", headers={"x-telegram-id": telegram_uid}
    ) as response:
        if response.status != 200:
            raise UnexpectedBackendError()

        payload = await response.json()

        if payload.get("borrower"):
            return (
                False,
                User.parse_obj(payload.get("borrower")),
                Book.parse_obj(payload.get("book")),
            )

        return (
            True,
            User.parse_obj(payload.get("owner")),
            Book.parse_obj(payload.get("book")),
        )
