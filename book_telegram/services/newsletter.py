from typing import List

from book_telegram.backend import book_backend
from book_telegram.models.user import User


class NewsletterService:
    def __init__(self):
        self.session = book_backend.get_session()

    async def get_mailing_list(self) -> List[User]:
        async with self.session.get("/users/", raise_for_status=True) as response:
            return [User.parse_obj(user) for user in await response.json()]
