import logging
import warnings
from typing import Union, Dict, Optional

import aiohttp
from aiogram import types
from yarl import URL

from book_telegram.backend import book_backend
from book_telegram.models.user import UserCreate, User


async def register_new_user(
    user_data: Union[UserCreate, Dict[str, str]]
) -> Optional[User]:
    """
    Регистрируем нового пользователя
    :param user_data: данные пользователя
    :return:
    """
    session: aiohttp.ClientSession = book_backend.session

    if isinstance(user_data, dict):
        user_data = UserCreate.parse_obj(user_data)

    # Регистрируем пользователя в системе
    async with session.post(URL("/users"), json={**user_data.dict()}) as response:
        if response.status != 201:
            warnings.warn(
                "Could not register user. \n"
                f"Request data: {str(user_data.dict())}\n"
                f"Response data: {await response.json()}"
            )
            return
        return User.parse_obj(await response.json())


def create_initial_keyboard() -> types.ReplyKeyboardMarkup:
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)

    keyboard.add(types.KeyboardButton(text="📚 Список книг"))
    keyboard.add(types.KeyboardButton(text="➕ Добавить книгу"))
    keyboard.add(types.KeyboardButton(text="📖 Мои книги"))

    return keyboard


async def get_user_by_telegram_uid(telegram_uid: str) -> Optional[User]:
    session: aiohttp.ClientSession = book_backend.get_session()

    async with session.get(
        URL("/users").with_query({"telegram_uid": telegram_uid})
    ) as response:
        if response.status != 200:
            logging.error(f"Не удалось найти пользователя с telegram id {telegram_uid}")
            return

        return User.parse_obj(await response.json())
