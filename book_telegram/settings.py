from typing import Optional

import aiohttp
from aiogram import types
from aiogram.utils.helper import Item
from pydantic import BaseSettings, AnyHttpUrl


class Settings(BaseSettings):
    token: str
    backend_base_url: AnyHttpUrl = "http://127.0.0.1:8000/"
    proxy: Optional[str] = None
    proxy_login: Optional[str] = None
    proxy_password: Optional[str] = None


class BotSettings(BaseSettings):
    token: str
    parse_mode: Optional[str] = "MARKDOWN"
    proxy: Optional[str] = None
    proxy_auth: Optional[aiohttp.BasicAuth] = None


settings = Settings(_env_file=".env", _env_file_encoding="utf-8")

bot_settings = BotSettings(
    token=settings.token,
    proxy=settings.proxy,
    proxy_auth=aiohttp.BasicAuth(
        login=settings.proxy_login, password=settings.proxy_password
    )
    if settings.proxy_login and settings.proxy_password
    else None,
)
