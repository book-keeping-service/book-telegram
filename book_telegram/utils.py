def mention_telegram_user_in_markdown(full_name, telegram_id: str):
    return f"[{full_name}](tg://user?id={telegram_id})"


def convert_isbn_to_numbers(raw_isbn: str) -> str:
    return raw_isbn.replace("-", "")
